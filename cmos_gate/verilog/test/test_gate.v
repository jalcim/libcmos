module test_gate;
   reg e1, e2, e3;
   wire out_not, out_nor, out_nand, out_and, out_or, out_xor, out_xnor;

   gate_not  not0 (out_not ,  e1);
   gate_nor  nor1 (out_nor , {e1, e2});
   gate_nand nand2(out_nand, {e1, e2});
   gate_and  and3 (out_and , {e1, e2});
   gate_or   or4  (out_or  , {e1, e2});
   gate_xor  xor5 (out_xor , {e1, e2});
   gate_xnor xnor6(out_xnor, {e1, e2});
   gate_aoi  aoi7 (out_aoi , {e1, e2, e3});
   gate_oai  oai8 (out_oai , {e1, e2, e3});

   initial
     begin
	$dumpfile("signal_test_gate_cmos.vcd");
        $dumpvars;
        $monitor("time %d\nnot\t%b\nnor\t%b\nnand\t%b\nand\t%b\nor\t%b\nxor\t%b\nxnor\t%b\naoi\t%b\noai\t%b\ne1\t%b\ne2\t%b\ne3\t%b\n",
		 $time, out_not, out_nor, out_nand,
		 out_and, out_or, out_xor, out_xnor,
		 out_aoi, out_oai,
		 e1, e2, e3);

	e1 <= 0;
	e2 <= 0;
	e3 <= 0;
	#10;
	e1 <= 1;
	#10;
	e1 <= 0;
	e2 <= 1;
	#10;
	e1 <= 1;
	#10;
	e1 <= 0;
	e2 <= 0;
	e3 <= 1;
	#10;
	e1 <= 1;
	#10;
	e1 <= 0;
	e2 <= 1;
	#10;
	e1 <= 1;
	#10;
	
     end
endmodule // test_gate_nor
