module gate_and_pp(out, in, vdd, vss);
   parameter SIZE = 2;

   input vdd, vss
   
   input [SIZE-1:0] in;
   output	    out;

   wire line;

   parallel_pmos #(.SIZE(2)) pmos_array({line, line},
					{SIZE{vdd}},
					in);

   serial_nmos #(.SIZE(2)) nmos_array(line,
				      vss,
				      in);

   gate_not_pp invout(out, line, vdd, vss);

endmodule
