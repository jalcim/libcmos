module gate_or(out, in);
   parameter SIZE = 2;
   input [SIZE-1:0] in;
   output	    out;

   wire 	    vdd;
   wire 	    vss;

   wire line;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   serial_pmos #(.SIZE(2)) pmos_array(line,
				      vdd,
				      in);

   parallel_nmos #(.SIZE(2)) nmos_array({line, line},
					{SIZE{vss}},
					in);

   gate_not invout(out, line);

endmodule
