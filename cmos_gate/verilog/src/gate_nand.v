module gate_nand(out, in);
   parameter SIZE = 2;
   input [SIZE-1:0] in;
   output	    out;

   wire 	    vdd;
   wire 	    vss;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   parallel_pmos #(.SIZE(2)) pmos_array({out, out},
					{SIZE{vdd}},
					in);
				      
   serial_nmos #(.SIZE(2)) nmos_array(out,
				      vss,
				      in);
endmodule
