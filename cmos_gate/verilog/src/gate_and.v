module gate_and(out, in);
   parameter SIZE = 2;
   input [SIZE-1:0] in;
   output	    out;

   wire 	    vdd;
   wire 	    vss;

   wire line;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   parallel_pmos #(.SIZE(2)) pmos_array({line, line},
					{SIZE{vdd}},
					in);

   serial_nmos #(.SIZE(2)) nmos_array(line,
				      vss,
				      in);

   gate_not invout(out, line);

endmodule
