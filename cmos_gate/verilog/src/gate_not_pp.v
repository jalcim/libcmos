module gate_not_pp(out, in, vdd, vss);

   input vdd, vss;
   
   input in;
   output out;

   wire vdd;
   wire vss;

   t_pmos pmos0(out, vdd, in);
   t_nmos nmos0(out, vss, in);

endmodule
