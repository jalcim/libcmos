module gate_nor(out, in);
   parameter SIZE = 2;
   input [SIZE-1:0] in;
   output	    out;

   wire 	    vdd;
   wire 	    vss;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   serial_pmos #(.SIZE(2)) pmos_array(out,
				      vdd,
				      in);

   parallel_nmos #(.SIZE(2)) nmos_array({out, out},
					{SIZE{vss}},
					in);

endmodule
