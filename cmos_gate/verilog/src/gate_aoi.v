module gate_aoi(out, in);
   input [2:0] in;
   output      out;

   wire        vdd;
   wire        vss;

   wire [1:0]  L;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   t_pmos pmos0 (L[0], vdd , in[0]);
   t_pmos pmos1 (L[0], vdd , in[1]);
   t_pmos pmos2 (out , L[0], in[2]);

   t_nmos nmos1 (L[1], vss , in[0]);
   t_nmos nmos0 (out , L[1], in[1]);
   t_nmos nmos2 (out , vss , in[2]);
endmodule
