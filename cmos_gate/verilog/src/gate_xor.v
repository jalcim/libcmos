module gate_xor(out, in);
   parameter SIZE = 2;
   input [SIZE-1:0] in;
   output 	    out;

   wire 	    vdd;
   wire 	    vss;

   wire 	    line;
   wire [SIZE-1:0]  inv;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   parallel_not #(.SIZE(SIZE))parallel_not(inv[SIZE-1:0], in[SIZE-1:0]);

   serial_pmos #(.SIZE(2)) pmos_array0(out,
				      vdd,
				      {in[0], inv[1]});

   serial_pmos #(.SIZE(2)) pmos_array1(out,
				      vdd,
				      {inv[0], in[1]});

   serial_nmos #(.SIZE(2)) nmos_array0(out,
				      vss,
				      in[1:0]);

   serial_nmos #(.SIZE(2)) nmos_array1(out,
				      vss,
				      inv[1:0]);

endmodule // gate_xor
