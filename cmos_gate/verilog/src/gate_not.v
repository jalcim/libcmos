`include "../../../supply/verilog/vdd/vdd5.v"
`include "../../../supply/verilog/vss/vss0.v"

module gate_not(out, in);
   input in;
   output out;

   wire vdd;
   wire vss;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   t_pmos pmos0(out, vdd, in);
   t_nmos nmos0(out, vss, in);

endmodule
