#!/bin/sh

mkdir bin

iverilog src/* \
	 test/test_gate.v \
	 ~/my_pdk/analoglib/cmos/cmos/verilog/src/* \
	 ~/my_pdk/analoglib/supply/verilog/* \
	 -o bin/eval_gate
