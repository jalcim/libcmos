yosys -import

read_verilog \
    ../cmos/verilog/src/serial_nmos.v \
    ../cmos/verilog/src/parallel_pmos.v \
    ../../supply/verilog/vss.v \
    ../../supply/verilog/vdd.v

read_verilog -lib \
    ../cmos/verilog/src/nmos.v \
    ../cmos/verilog/src/pmos.v

read_verilog verilog/src/gate_nand.v

procs
hierarchy
synth

write_spice spice/out.spice
