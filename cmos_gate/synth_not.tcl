yosys -import

read_verilog \
    ../../supply/verilog/vss.v \
    ../../supply/verilog/vdd.v

read_verilog -lib \
    ../cmos/verilog/src/nmos.v \
    ../cmos/verilog/src/pmos.v

read_verilog verilog/src/gate_not.v

procs
hierarchy
synth

write_spice spice/out_gate_not.spice
