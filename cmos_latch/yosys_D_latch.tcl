yosys -import

read_verilog \
    verilog/src/D_latch.v \
    verilog/src/sync_sr_nor.v

read_verilog -lib \
    ../cmos/verilog/src/nmos.v \
    ../cmos/verilog/src/pmos.v \
    ../../supply/verilog/vss.v \
    ../../supply/verilog/vdd.v

procs
hierarchy
clean
synth
clean

write_spice spice/synth/D_latch.spice
