module test_latch;
   reg S, R, CLK;
   wire [1:0] Q, _Q;

   sync_sr_nor s_sr_nor(CLK, S, R, Q[0], _Q[0]);

   D_latch D(CLK, S, Q[1], _Q[1]);

   initial
     begin
	$dumpfile("signal_test_async_sr_latch");
        $dumpvars;
	$display("sync nor D\n");
        $monitor("time\t%d\nCLK\t%d\nS\t%b\nR\t%b\nQ\t%b\n_Q\t%b\n",
		 $time, CLK, S, R, Q, _Q);

	S = 0;
	R = 0;
	CLK = 0;
	#10;
	S = 1;
	R = 0;
	CLK = 1;
	#10;
	S = 1;
	R = 0;
	CLK = 0;
	#10;
	S = 0;
	R = 1;
	CLK = 1;
	#10;
	S = 0;
	R = 1;
	CLK = 0;
	#10;
////////////
	$display ("bad\n");
	S = 0;
	R = 0;
	CLK = 0;
	#10;
	S = 1;
	R = 1;
	CLK = 0;
	#10;
	S = 1;
	R = 1;
	CLK = 1;
	#10;
	S = 0;
	R = 0;
	CLK = 1;
	#10;
	S = 0;
	R = 1;
	CLK = 1;
	#10;
	S = 0;
	R = 1;
	CLK = 0;
	#10;	
     end
endmodule
