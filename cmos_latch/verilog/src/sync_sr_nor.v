module sync_sr_nor (CLK, S, R, Q, _Q);
   input S,   R, CLK;
   output Q, _Q;

   wire [5:0] L;

   wire vdd;
   wire vss;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   //top left
   t_pmos pmos0 (L[0], vdd, S);
   t_pmos pmos1 (L[0], vdd, CLK);
   t_pmos pmos2 (L[1], L[0], L[3]);//retro-active

   //top right
   t_pmos pmos3 (L[2], vdd, R);
   t_pmos pmos4 (L[2], vdd, CLK);
   t_pmos pmos5 (L[3], L[2], L[1]);//retro-active

   /////////////////////////////

   //bottom left
   t_nmos nmos1 (L[4], vss, CLK);
   t_nmos nmos0 (L[1], L[4], S);
   
   t_nmos nmos2 (L[1], vss, L[3]);//retro-active

   //bottom right
   t_nmos nmos4 (L[5], vss, CLK);
   t_nmos nmos3 (L[3], L[5], R);
   
   t_nmos nmos5 (L[3], vss, L[1]);//retro-active

   assign _Q = L[1];
   assign Q = L[3];
endmodule

