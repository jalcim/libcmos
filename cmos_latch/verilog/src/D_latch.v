module D_latch(CLK, D, Q, _Q);
   input CLK, D;
   output Q, _Q;

   wire   _D;

   wire   vdd;
   wire   vss;

   VDD5 alim1 (vdd);
   VSS0 alim2 (vss);

   t_pmos invSp (_D, vdd, D);
   t_nmos invSn (_D, vss, D);

   sync_sr_nor sr_nor (CLK, D, _D, Q, _Q);

endmodule
