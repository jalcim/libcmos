`ifndef __SERIAL_NMOS__
 `define __SERIAL_NMOS__

module serial_nmos(drain, source, gate);
   parameter SIZE = 3;
   output 	    drain;
   input 	    source;
   input [SIZE-1:0] gate;

   wire 	    line;
   if (SIZE > 1)
     begin
	serial_nmos #(.SIZE(SIZE-1)) recall(line, source, gate[SIZE-1:1]);
	t_nmos inst(drain, line, gate[0]);
     end
   else
     t_nmos inst(drain, source, gate[0]);



endmodule

`endif
