.option scale=1E-6
.include wrapper/nmos.sp

Vin0 SOURCE 0 DC 1.8v
Vin1 GATE 0 pulse(0v 1.8v 0 0ns 0ns 20ns 40ns)

.param pdk=0
X0 DRAIN1 GATE 0 0 t_nmos wrapper=0

.param pdk=1
X0 DRAIN2 GATE 0 0 t_nmos wrapper=1

.tran 1ns 400ns
.control
	run
	plot v(SOURCE)
	plot v(GATE)
	plot v(DRAIN1)
	plot v(DRAIN2)
.endc

.end