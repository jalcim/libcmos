.option scale=1E-6

.IF (process == 0)
        .include "pm3/sky130_fd_pr__nfet_01v8__tt.pm3.spice"
.ELSEIF (process == 1)
	.include "pm3/sky130_fd_pr__nfet_01v8__ff.pm3.spice"
.ELSEIF (process == 2)
	.include "pm3/sky130_fd_pr__nfet_01v8__fs.pm3.spice"
.ELSEIF (process == 3)
	.include "pm3/sky130_fd_pr__nfet_01v8__leak.pm3.spice"
.ELSEIF (process == 4)
	.include "pm3/sky130_fd_pr__nfet_01v8__sf.pm3.spice"
.ELSEIF (process == 5)
	.include "pm3/sky130_fd_pr__nfet_01v8__ss.pm3.spice"
.ELSEIF (process == 6)
	.include "pm3/sky130_fd_pr__nfet_01v8__tt_leak.pm3.spice"
.ELSEIF (process == 7)
	.include "pm3/sky130_fd_pr__nfet_01v8__tt.pm3.spice"
.ENDIF

.param sky130_fd_pr__nfet_01v8__toxe_slope=3.443e-03
.param sky130_fd_pr__nfet_01v8__lint_slope=0.0
.param sky130_fd_pr__nfet_01v8__nfactor_slope=0.0
.param sky130_fd_pr__nfet_01v8__voff_slope=0.007
.param sky130_fd_pr__nfet_01v8__vth0_slope=3.356e-03
.param sky130_fd_pr__nfet_01v8__vth0_slope1=7.356e-03
.param sky130_fd_pr__nfet_01v8__wint_slope=0

.param sky130_fd_pr__pfet_01v8__toxe_slope=4.443e-03
.param sky130_fd_pr__pfet_01v8__toxe_slope1=6.443e-03
.param sky130_fd_pr__pfet_01v8__nfactor_slope=0.1
.param sky130_fd_pr__pfet_01v8__nfactor_slope1=0.1
.param sky130_fd_pr__pfet_01v8__voff_slope=0.0
.param sky130_fd_pr__pfet_01v8__voff_slope1=0.0
.param sky130_fd_pr__pfet_01v8__vth0_slope=5.856e-03
.param sky130_fd_pr__pfet_01v8__vth0_slope1=7.356e-03

.option scale=1.0u

.param
+ sky130_fd_pr__pfet_01v8__lkvth0_diff = .0e-6
+ sky130_fd_pr__pfet_01v8__wlod_diff = .0e-6
+ sky130_fd_pr__pfet_01v8__lku0_diff = 0.0
+ sky130_fd_pr__pfet_01v8__kvsat_diff = 0.5
+ sky130_fd_pr__pfet_01v8__kvth0_diff = 3.29e-8
+ sky130_fd_pr__pfet_01v8__wkvth0_diff = .20e-6
+ sky130_fd_pr__pfet_01v8__ku0_diff = 4.5e-8
+ sky130_fd_pr__pfet_01v8__wku0_diff = .25e-6

.SUBCKT t_nmos_sky130 drain gate source bulk

X0 drain gate source bulk sky130_fd_pr__nfet_01v8

.ENDS