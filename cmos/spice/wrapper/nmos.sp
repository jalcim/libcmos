.IF (pdk == 0)
    .include ../generic/nmos.sp
.ELSEIF (pdk == 1)
    .include ../sky130/nmos.sp
.ENDIF

.param process = 0

.SUBCKT t_nmos drain gate source bulk wrapper=0
	.IF (wrapper == 0)
	    X0 drain gate source bulk t_nmos_generic
	.ELSEIF (wrapper == 1)
	    X0 drain gate source bulk t_nmos_sky130
	.ENDIF
.ENDS

