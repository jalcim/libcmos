yosys -import

read_verilog \
    verilog/src/serial_nmos.v \
    verilog/src/parallel_nmos.v \
    verilog/src/serial_pmos.v \
    verilog/src/parallel_pmos.v

read_verilog -lib \
    verilog/src/nmos.v \
    verilog/src/pmos.v

procs
hierarchy
synth

write_spice spice/synth/out.spice
