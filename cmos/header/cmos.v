`ifndef __T_PMOS__
 `define __T_PMOS__
 `include "../../cmos/verilog/src/pmos.v"
`endif

`ifndef __T_NMOS__
 `define __T_NMOS__
 `include "../../cmos/verilog/src/nmos.v"
`endif
