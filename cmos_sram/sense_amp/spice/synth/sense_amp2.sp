* SPICE netlist generated by Yosys 0.29+40 (git sha1 8596c5ce4, gcc 10.2.1-6 -fPIC -Os)

.SUBCKT sense_amp_2 left_in right_in left_out right_out Ysel
X0 left_line left_in right_in t_nmos
X1 right_line right_in left_in t_nmos
X2 left_out left_line Ysel t_pmos
X3 right_out right_line Ysel t_pmos
.ENDS sense_amp_2

************************
* end of SPICE netlist *
************************

