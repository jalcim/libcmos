`include "../../cmos/verilog/src/nmos.v"
`include "../../cmos/verilog/src/pmos.v"

module sense_amp_2(left_in, right_in, left_out, right_out, Ysel);
   input left_in, right_in;
   input Ysel;
   output left_out, right_out;

   wire   left_line, right_line;

   t_nmos nmos_left (left_line, left_in , right_in);
   t_nmos nmos_right(right_line, right_in,  left_in);

   t_pmos pmos_left(left_out, left_line, Ysel);
   t_pmos pmos_right(right_out, right_line, Ysel);

endmodule
