`include "../../../supply/verilog/vdd/vdd5.v"
`include "../../../supply/verilog/vss/vss0.v"

`include "../../cmos/verilog/src/nmos.v"
`include "../../cmos/verilog/src/pmos.v"

`ifndef __GATE_NOT__
 `include "../../cmos_gate/verilog/src/gate_not.v"
 `define __GATE_NOT__
`endif

`include "../../cmos_gate/verilog/src/gate_not_pp.v"

module sense_amp(DL, _DL, S, _S);
   inout DL, _DL;
   input S, _S;

   wire  [1:0]line;

   wire       vdd, vss;

   VDD5 alim1(vdd);
   VSS0 alim2(vss);

   t_pmos pmos_top(line[1], vdd, _S);
   t_nmos nmos_bot(line[0], vss,  S);

   gate_not_pp inv_left (DL , _DL, line[1], line[0]);
   gate_not_pp inv_right(_DL, DL , line[1], line[0]);
endmodule
