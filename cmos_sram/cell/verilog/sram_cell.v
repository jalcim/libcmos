`include "../../cmos/verilog/src/nmos.v"
`include "../../cmos/verilog/src/pmos.v"

`ifndef __GATE_NOT__
 `include "../../cmos_gate/verilog/src/gate_not.v"
 `define __GATE_NOT__
`endif

module sram_cell(select, in_left, in_right);
   input select;
   inout in_left, in_right;

   wire   right_inv_in, left_inv_in;
   wire   left_inv_out, right_inv_out;

   t_nmos nmos_rw_left (right_inv_in, in_left, select);
   t_nmos nmos_rw_right(left_inv_in , in_right, select);

   gate_not inv_left (right_inv_in , left_inv_in );
   gate_not inv_right(left_inv_in, right_inv_in);
   
endmodule
