`include "../cell/verilog/sram_cell.v"
`include "../sense_amp/verilog/sense_amp_1.v"
`include "../preload/verilog/preload.v"

`ifndef __GATE_NOT__
 `include "../../cmos_gate/verilog/src/gate_not.v"
 `define __GATE_NOT__
`endif

module sram_1bit(DL, _DL, S, _S, P);

   input DL, _DL, S, _S, P;

   sram_cell cell0(1, DL, _DL);
   sense_amp sense_amp0(DL, _DL, S, _S);
   preload preload0(DL, _DL, P);

endmodule
