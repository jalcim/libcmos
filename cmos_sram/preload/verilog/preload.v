`include "../../../supply/verilog/vdd/vdd2_5.v"
`include "../../cmos/verilog/src/nmos.v"

module preload(DL, _DL, P);
   inout DL, _DL;
   input P;

   wire  vdd;

   VDD2_5 alim1(vdd);

   t_nmos nmos_top  (_DL, DL,  P);
   t_nmos nmos_left ( DL, vdd,  P);
   t_nmos nmos_right(_DL, vdd,  P);

endmodule
