* SPICE netlist generated by Yosys 0.29+40 (git sha1 8596c5ce4, gcc 10.2.1-6 -fPIC -Os)

.SUBCKT preload_1 DL _DL P
X0 _DL vdd P t_nmos
X1 DL vdd P t_nmos
X2 _DL DL P t_nmos
X3 vdd VDD2_5
.ENDS preload_1

************************
* end of SPICE netlist *
************************

