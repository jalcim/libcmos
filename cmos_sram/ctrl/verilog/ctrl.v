`include "../../cmos/header/cmos.v"

`ifndef __GATE_NOT__
 `define __GATE_NOT__
 `include "../../cmos_gate/verilog/src/gate_not.v"
`endif

`include "../full/verilog/sram_1bit.v"

module sram_ctrl(data, S, P, write);
   input data, S, write;

   wire  DL, _DL, _data, _S;

   gate_not inv_DL (_data, data);
   gate_not inv_S (_S, S);

   t_nmos nmos_write1( DL,  data, write);
   t_nmos nmos_write2(_DL, _data, write);

   sram_1bit sram_1bit(DL, _DL, S, _S, P);
endmodule
